package la.rat.trycard.model;

import org.json.simple.JSONObject;

public class Card {
    private final int id;
    private final String name;
    private final String imagePath;
    private boolean tapped;
    private boolean upsideDown;
    private int plusAttack;
    private int plusDefense;
    private int xPos;
    private int yPos;
    private int zPos;

    public Card(int id, String name, String imagePath, boolean tapped, boolean upsideDown, int plusAttack, int plusDefense, int xPos, int yPos, int zPos) {
        this.id = id;
        this.name = name;
        this.imagePath = imagePath;
        this.tapped = tapped;
        this.upsideDown = upsideDown;
        this.plusAttack = plusAttack;
        this.plusDefense = plusDefense;
        this.xPos = xPos;
        this.yPos = yPos;
        this.zPos = zPos;
    }

    public JSONObject toJSONObject() {
        JSONObject jsonObject = new JSONObject();

        jsonObject.put("id", this.id);
        jsonObject.put("name", this.name);
        jsonObject.put("image_path", this.imagePath);
        jsonObject.put("tapped", this.tapped);
        jsonObject.put("upside_down", this.upsideDown);
        jsonObject.put("plus_attack", this.plusAttack);
        jsonObject.put("plus_defense", this.plusDefense);
        jsonObject.put("x_pos", this.xPos);
        jsonObject.put("y_pos", this.yPos);
        jsonObject.put("z_pos", this.zPos);

        return jsonObject;
    }
}
