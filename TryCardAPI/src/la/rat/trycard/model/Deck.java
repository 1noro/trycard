package la.rat.trycard.model;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.List;

public class Deck {
    private final int id;
    private final String name;
    private final List<Card> cardsInLibrary;
    private final List<Card> cardsInHand;
    private final List<Card> cardsInGame;

    public Deck(int id, String name, List<Card> cardsInLibrary, List<Card> cardsInHand, List<Card> cardsInGame) {
        this.id = id;
        this.name = name;
        this.cardsInLibrary = cardsInLibrary;
        this.cardsInHand = cardsInHand;
        this.cardsInGame = cardsInGame;
    }

    public JSONObject toJSONObject() {
        JSONObject jsonObject = new JSONObject();

        jsonObject.put("id", this.id);
        jsonObject.put("name", this.name);

        JSONArray jsonArrayCards = new JSONArray();
        for (Card card : this.cardsInLibrary)
            jsonArrayCards.add(card.toJSONObject());
        jsonObject.put("cards_in_library", jsonArrayCards);

        jsonArrayCards = new JSONArray();
        for (Card card : this.cardsInHand)
            jsonArrayCards.add(card.toJSONObject());
        jsonObject.put("cards_in_hand", jsonArrayCards);

        jsonArrayCards = new JSONArray();
        for (Card card : this.cardsInGame)
            jsonArrayCards.add(card.toJSONObject());
        jsonObject.put("cards_in_game", jsonArrayCards);

        return jsonObject;
    }
}
