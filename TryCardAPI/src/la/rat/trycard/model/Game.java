package la.rat.trycard.model;

import org.json.simple.JSONObject;

public class Game {
    private final int id;
    private final Deck myDeck;
    private final Deck opponentDeck;
    private final int myLifes;
    private final int opponentLifes;

    public Game(int id, Deck myDeck, Deck opponentDeck, int myLifes, int opponentLifes) {
        this.id = id;
        this.myDeck = myDeck;
        this.opponentDeck = opponentDeck;
        this.myLifes = myLifes;
        this.opponentLifes = opponentLifes;
    }

    public JSONObject toJSONObject() {
        JSONObject jsonObject = new JSONObject();

        jsonObject.put("id", this.id);
        jsonObject.put("my_deck", this.myDeck.toJSONObject());
        jsonObject.put("opponent_deck", this.opponentDeck.toJSONObject());
        jsonObject.put("my_lifes", this.myLifes);
        jsonObject.put("opponent_lifes", this.opponentLifes);

        return jsonObject;
    }

}
