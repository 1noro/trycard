package la.rat.trycard;

import la.rat.trycard.resources.*;
import org.restlet.Application;
import org.restlet.Component;
import org.restlet.Context;
import org.restlet.Restlet;
import org.restlet.data.Protocol;
import org.restlet.routing.Router;
import org.restlet.engine.application.CorsFilter;

import java.util.Arrays;
import java.util.HashSet;

public class Main extends Application {

    public Main(Context context) {
        super(context);
    }

    public static void main(String[] args) throws Exception {
        final Component component = new Component();
        component.getServers().add(Protocol.HTTP, 8080);
        Main server = new Main(component.getContext().createChildContext());
        component.getDefaultHost().attach(server);
        component.start();
    }

    @Override
    public Restlet createInboundRoot() {
        Router router = new Router(getContext().createChildContext());

        // endpoints GLOBALES
        router.attach("/health-check", HealthCheckResource.class);
        router.attach("/game", GameStatusResource.class);
        router.attach("/game/{game_id}", GameResource.class);
        // endpoints de ACCIONES
        router.attach("/game/{game_id}/card/{card_id}/status/{new_status}", CardStatusResource.class);
        router.attach("/game/{game_id}/card/{card_id}/tap/{tap_position}", CardTapResource.class);
        router.attach("/game/{game_id}/card/{card_id}/upsidedown/{upside_down_position}", CardUpsideDownResource.class);
        router.attach("/game/{game_id}/card/{card_id}/move", CardMoveResource.class);
        router.attach("/game/{game_id}/card/{card_id}/plus", CardPlusesResource.class);
        router.attach("/game/{game_id}/lifes", PlayerLifesResource.class);

        CorsFilter corsFilter = new CorsFilter(getContext(), router);
//        corsFilter.setAllowedOrigins(new HashSet(Arrays.asList("trycard.rat.la")));
        corsFilter.setAllowedOrigins(new HashSet(Arrays.asList("*")));
        corsFilter.setAllowedCredentials(true);

//        return router;
        return corsFilter;
    }

}
