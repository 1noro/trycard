package la.rat.trycard.exceptions;

public class MyHTTPException extends Exception {
    private final int errorCode;
    private String extraText = null;
    public MyHTTPException(int errorCode) {
        this.errorCode = errorCode;
    }

    public MyHTTPException(int errorCode, String extraText) {
        this.errorCode = errorCode;
        this.extraText = extraText;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getExtraText() {
        return extraText;
    }
}
