package la.rat.trycard.dataproviders;

public class DataProviderFactory {
    public static DataProvider getDataProvider() {
        return new SQLiteDataProvider();
    }
}
