package la.rat.trycard.dataproviders;

import la.rat.trycard.exceptions.MyHTTPException;
import la.rat.trycard.model.Card;
import la.rat.trycard.model.Deck;
import la.rat.trycard.model.Game;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SQLiteDataProvider implements DataProvider {

    public Connection getConnection() throws SQLException {
        // db parameters
        String url = "jdbc:sqlite:/opt/jarchivos/trycard.db";
        // create a connection to the database
        return DriverManager.getConnection(url);
    }

    // endpoints GLOBALES
    // contemplar errores mas adelante
    @Override
    public Integer createGame(JSONArray gameDecksJSON) throws MyHTTPException {
        List<Integer> deckIds = new ArrayList<>();
        for (Object singleDeckObj : gameDecksJSON) {
            JSONObject singleDeckJSON = (JSONObject) singleDeckObj;
            String deckName = ((String) singleDeckJSON.get("name")).replace("'", "&lsquo;"); // OJO, esto puede que sea necesario en otro momento
            Connection conn = null;
            Statement stmt = null;
            try {
                conn = getConnection();
                stmt = conn.createStatement();
                String sql = "INSERT INTO DECK (name) VALUES ('" + deckName + "');";
                stmt.executeUpdate(sql); // INSERTAMOS EL DECK

                sql = "SELECT last_insert_rowid();";
                ResultSet resultSet = stmt.executeQuery(sql); // OBTENEMOS EL deckId
                int deckId = resultSet.getInt(1);
                deckIds.add(deckId);

                // -----------------------------------------------------------
                // FORMA BUENA DE HACERLO, NO IMPLEMENTADA PARA SQLITE
                /*String[] returnColumns = {"id"};
                stmt.executeUpdate(sql, returnColumns); // INSERTAMOS EL DECK

                // obtenemos el deckId de la tupla que acabamos de insrertar
                ResultSet resultSet = stmt.getResultSet();
                int deckId = resultSet.getInt(1);*/
                // -----------------------------------------------------------

                for (Object cardObj : (JSONArray) singleDeckJSON.get("main")) {
                    JSONObject cardJSON = (JSONObject) cardObj;
                    String cardName = ((String) cardJSON.get("name")).replace("'", "&lsquo;"); // OJO, esto puede que sea necesario en otro momento
                    String cardImagePath = (String) cardJSON.get("image_path");
                    int cardQuantity = ((Long) cardJSON.get("quantity")).intValue();
                    for (int i = 0; i < cardQuantity; i++) {
                        sql = "INSERT INTO CARD (name, image_path, deck_id) VALUES ('" + cardName + "', '" + cardImagePath + "', " + deckId + ");";
//                        System.out.println(sql);
                        stmt.executeUpdate(sql); // INSERTAMOS CADA UNA DE LAS CARTAS
                    }
                }

            } catch(SQLException se) {
                //Handle errors for JDBC
                se.printStackTrace();
                throw new MyHTTPException(500);
            } finally {
                //finally block used to close resources
                try {
                    if (stmt != null) stmt.close();
                } catch(SQLException se) {
                    se.printStackTrace();
                }

                try {
                    if (conn != null) conn.close();
                } catch (SQLException se) {
                    se.printStackTrace();
                }
            }
        }

        Connection conn = null;
        Statement stmt = null;
        try {
            conn = getConnection();
            stmt = conn.createStatement();
            String sql = "INSERT INTO GAME (player1_deck_id, player2_deck_id) VALUES (" + deckIds.get(0) + ", " + deckIds.get(1) + ");";
            stmt.executeUpdate(sql); // INSERTAMOS EL DECK

            sql = "SELECT last_insert_rowid();";
            ResultSet resultSet = stmt.executeQuery(sql); // OBTENEMOS EL deckId
            return resultSet.getInt(1);

            // -----------------------------------------------------------
            // FORMA BUENA DE HACERLO, NO IMPLEMENTADA PARA SQLITE
            /*String[] returnColumns = {"id"};
            stmt.executeUpdate(sql, returnColumns); // INSERTAMOS EL DECK

            // obtenemos el deckId de la tupla que acabamos de insrertar
            ResultSet resultSet = stmt.getResultSet();
            int deckId = resultSet.getInt(1);*/
            // -----------------------------------------------------------

        } catch(SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
            throw new MyHTTPException(500);
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null) stmt.close();
            } catch(SQLException se) {
                se.printStackTrace();
            }

            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    // contemplar errores mas adelante
    @Override
    public int endGame(int gameId) throws MyHTTPException {
        Connection conn = null;
        Statement stmt = null;
        try {
            conn = getConnection();
            stmt = conn.createStatement();
            String sql = "SELECT player1_deck_id, player2_deck_id FROM GAME WHERE id = " + gameId + ";";
            ResultSet resultSet = stmt.executeQuery(sql); // Obtenemos los datos del GAME
            int deckId1 = resultSet.getInt(1);
            int deckId2 = resultSet.getInt(2);
            sql = "DELETE FROM CARD WHERE deck_id = " + deckId1 + " OR deck_id = " + deckId2 + ";";
            stmt.executeUpdate(sql);
            sql = "DELETE FROM GAME WHERE id = " + gameId + ";";
            stmt.executeUpdate(sql);
            sql = "DELETE FROM DECK WHERE id = " + deckId1 + " OR id = " + deckId2 + ";";
            stmt.executeUpdate(sql);
        } catch(SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
            throw new MyHTTPException(500);
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null) stmt.close();
            } catch(SQLException se) {
                se.printStackTrace();
            }

            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return 200;
    }

    // contemplar errores mas adelante
    private Deck getDeckById(Statement stmt, int deckId) throws SQLException {
        // obtenemos las cartas de myDeck
        String sql = "SELECT id, name, image_path, tapped, upside_down, plus_attack, plus_defense, x_pos, y_pos, z_pos, status FROM CARD WHERE deck_id = " + deckId + ";";
        ResultSet resultSet = stmt.executeQuery(sql); // obtenemos las cartas del deck

        List<Card> cardsInLibrary = new ArrayList<>();
        List<Card> cardsInHand = new ArrayList<>();
        List<Card> cardsInGame = new ArrayList<>();
        while (resultSet.next()) {
            int id = resultSet.getInt(1);
            String name = resultSet.getString(2);
            String imagePath = resultSet.getString(3);
            boolean tapped = resultSet.getBoolean(4);
            boolean upsideDown = resultSet.getBoolean(5);
            int plusAttack = resultSet.getInt(6);
            int plusDefense = resultSet.getInt(7);
            int xPos = resultSet.getInt(8);
            int yPos = resultSet.getInt(9);
            int zPos = resultSet.getInt(10);
            int status = resultSet.getInt(11);
            Card card = new Card(id,name, imagePath, tapped, upsideDown, plusAttack, plusDefense, xPos, yPos, zPos);
            switch (status) {
                case 0 -> cardsInLibrary.add(card);
                case 1 -> cardsInHand.add(card);
                case 2 -> cardsInGame.add(card);
            }
        }

        sql = "SELECT name FROM DECK WHERE id = " + deckId + ";";
        resultSet = stmt.executeQuery(sql); // obtenemos el nombre del deck
        String name = resultSet.getString(1);

        return new Deck(deckId, name, cardsInLibrary, cardsInHand, cardsInGame);
    }

    // contemplar errores mas adelante
    @Override
    public Game getGame(int gameId, int playerId) throws MyHTTPException {
        int myDeckId;
        int opponentDeckId;
        int myLifes;
        int opponentLifes;
        Connection conn = null;
        Statement stmt = null;
        try {
            conn = getConnection();
            stmt = conn.createStatement();
            String sql = "SELECT player1_deck_id, player2_deck_id, player1_lifes, player2_lifes FROM GAME WHERE id = " + gameId + ";";
            ResultSet resultSet = stmt.executeQuery(sql); // Obtenemos los datos del GAME
            int deckId1 = resultSet.getInt(1);
            int deckId2 = resultSet.getInt(2);
            int player1Lifes = resultSet.getInt(3);
            int player2Lifes = resultSet.getInt(4);

            if (playerId == 1) {
                myDeckId = deckId1;
                opponentDeckId = deckId2;
                myLifes = player1Lifes;
                opponentLifes = player2Lifes;
            } else {
                myDeckId = deckId2;
                opponentDeckId = deckId1;
                myLifes = player2Lifes;
                opponentLifes = player1Lifes;
            }

            Deck myDeck = getDeckById(stmt, myDeckId);
            Deck opponentDeck = getDeckById(stmt, opponentDeckId);

            return new Game(gameId, myDeck, opponentDeck, myLifes, opponentLifes);

        } catch(SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
            throw new MyHTTPException(500);
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null) stmt.close();
            } catch(SQLException se) {
                se.printStackTrace();
            }

            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    // endpoints de ACCIONES
    // contemplar errores mas adelante
    private void resetCardAttrs(Statement stmt, int cardId) throws SQLException {
        String sql = "UPDATE CARD SET tapped = 0, upside_down = 0, plus_attack = 0, plus_defense = 0, x_pos = 0, y_pos = 0, z_pos = 0 WHERE id = " + cardId + ";";
        stmt.executeUpdate(sql); // Actualizamos el estado de la carta
    }

    // contemplar errores mas adelante
    @Override
    public Game changeCardStatus(int gameId, int playerId, int cardId, int newStatus) throws MyHTTPException {
        Connection conn = null;
        Statement stmt = null;
        try {
            conn = getConnection();
            stmt = conn.createStatement();
            if (newStatus == 2) {
                // si la carta pasa a estar inGame (status == 2) se resetean todos sus atributos
                resetCardAttrs(stmt, cardId);
            }
            String sql = "UPDATE CARD SET status = " + newStatus + " WHERE id = " + cardId + ";";
            stmt.executeUpdate(sql); // Actualizamos el estado de la carta
            return getGame(gameId, playerId);
        } catch(SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
            throw new MyHTTPException(500);
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null) stmt.close();
            } catch(SQLException se) {
                se.printStackTrace();
            }

            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    @Override
    public Game tapCard(int gameId, int playerId, int cardId, int tapPosition) throws MyHTTPException {
        Connection conn = null;
        Statement stmt = null;
        try {
            conn = getConnection();
            stmt = conn.createStatement();
            String sql = "UPDATE CARD SET tapped = " + tapPosition + " WHERE id = " + cardId + ";";
            stmt.executeUpdate(sql); // Actualizamos el estado de la carta
            return getGame(gameId, playerId);
        } catch(SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
            throw new MyHTTPException(500);
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null) stmt.close();
            } catch(SQLException se) {
                se.printStackTrace();
            }

            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    @Override
    public Game setUpsideDownCard(int gameId, int playerId, int cardId, int upsideDown) throws MyHTTPException {
        Connection conn = null;
        Statement stmt = null;
        try {
            conn = getConnection();
            stmt = conn.createStatement();
            String sql = "UPDATE CARD SET upside_down = " + upsideDown + " WHERE id = " + cardId + ";";
            stmt.executeUpdate(sql); // Actualizamos el estado de la carta
            return getGame(gameId, playerId);
        } catch(SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
            throw new MyHTTPException(500);
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null) stmt.close();
            } catch(SQLException se) {
                se.printStackTrace();
            }

            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    @Override
    public Game setCardPluses(int gameId, int playerId, int cardId, int plusAttack, int plusDefense) throws MyHTTPException {
        Connection conn = null;
        Statement stmt = null;
        try {
            conn = getConnection();
            stmt = conn.createStatement();
            String sql = "UPDATE CARD SET plus_attack = plus_attack + " + plusAttack + ", plus_defense = plus_defense + " + plusDefense + " WHERE id = " + cardId + ";";
            stmt.executeUpdate(sql); // Actualizamos el estado de la carta
            return getGame(gameId, playerId);
        } catch(SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
            throw new MyHTTPException(500);
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null) stmt.close();
            } catch(SQLException se) {
                se.printStackTrace();
            }

            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    @Override
    public Game moveCard(int gameId, int playerId, int cardId, int xPos, int yPos, int zPos) throws MyHTTPException {
        Connection conn = null;
        Statement stmt = null;
        try {
            conn = getConnection();
            stmt = conn.createStatement();
            String sql = "UPDATE CARD SET x_pos = " + xPos + ", y_pos = " + yPos + ", z_pos = " + zPos + " WHERE id = " + cardId + ";";
            stmt.executeUpdate(sql); // Actualizamos el estado de la carta
            return getGame(gameId, playerId);
        } catch(SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
            throw new MyHTTPException(500);
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null) stmt.close();
            } catch(SQLException se) {
                se.printStackTrace();
            }

            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    @Override
    public Game setPlayerLifes(int gameId, int playerId, int plusLifes) throws MyHTTPException {
        Connection conn = null;
        Statement stmt = null;
        try {
            conn = getConnection();
            stmt = conn.createStatement();
            String sql;
            if (playerId == 1) {
                sql = "UPDATE GAME SET player1_lifes = player1_lifes + " + plusLifes + " WHERE id = " + gameId + ";";
            } else {
                sql = "UPDATE GAME SET player2_lifes = player2_lifes + " + plusLifes + " WHERE id = " + gameId + ";";
            }
            stmt.executeUpdate(sql); // Actualizamos el estado de la carta
            return getGame(gameId, playerId);
        } catch(SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
            throw new MyHTTPException(500);
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null) stmt.close();
            } catch(SQLException se) {
                se.printStackTrace();
            }

            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }
}
