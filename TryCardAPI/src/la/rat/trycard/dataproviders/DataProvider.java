package la.rat.trycard.dataproviders;

import la.rat.trycard.exceptions.MyHTTPException;
import la.rat.trycard.model.Game;
import org.json.simple.JSONArray;

public interface DataProvider {
    // Iniciar el juego
    Integer createGame(JSONArray gameDeckJSON) throws MyHTTPException;
    // Finalizar el juego
    int endGame(int gameId) throws MyHTTPException;
    // Obtener la vista del juego desde el punto de vista del jugador indicado
    Game getGame(int gameId, int playerId) throws MyHTTPException;

    // Cambiar el estado de una carta
    Game changeCardStatus(int gameId, int playerId, int cardId, int newStatus) throws MyHTTPException;
    Game tapCard(int gameId, int playerId, int cardId, int tap) throws MyHTTPException;
    Game setUpsideDownCard(int gameId, int playerId, int cardId, int upsideDown) throws MyHTTPException;
    Game setCardPluses(int gameId, int playerId, int cardId, int plusAttack, int plusDefense) throws MyHTTPException;
    Game moveCard(int gameId, int playerId, int cardId, int xPos, int yPos, int zPos) throws MyHTTPException;
    Game setPlayerLifes(int gameId, int playerId, int plusLifes) throws MyHTTPException;
}

