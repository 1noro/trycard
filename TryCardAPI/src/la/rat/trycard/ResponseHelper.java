package la.rat.trycard;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.restlet.Response;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.StringRepresentation;

public class ResponseHelper {

    public static Response createSuccessResponse(Response response, JSONObject jsonObject) {
        response.setStatus(Status.SUCCESS_OK);
        StringRepresentation sRepresentation = new StringRepresentation(jsonObject.toJSONString());
        sRepresentation.setMediaType(MediaType.APPLICATION_JSON);
        response.setEntity(sRepresentation);
        return response;
    }

    public static Response createSuccessResponse(Response response, JSONArray jsonArray) {
        response.setStatus(Status.SUCCESS_OK);
        StringRepresentation sRepresentation = new StringRepresentation(jsonArray.toJSONString());
        sRepresentation.setMediaType(MediaType.APPLICATION_JSON);
        response.setEntity(sRepresentation);
        return response;
    }

    public static Response createSuccessResponse(Response response, String extra) {
        response.setStatus(Status.SUCCESS_OK);
        JSONObject httpResponse = new JSONObject();
        JSONObject httpData = new JSONObject();
        httpData.put("code", 200);
        httpData.put("description", "OK");
        httpData.put("extra", extra);
        httpResponse.put("http_response", httpData);
        StringRepresentation sRepresentation = new StringRepresentation(httpResponse.toJSONString());
        sRepresentation.setMediaType(MediaType.APPLICATION_JSON);
        response.setEntity(sRepresentation);
        return response;
    }

    public static Response createCreatedResponse(Response response) {
        response.setStatus(Status.SUCCESS_CREATED);
        JSONObject httpResponse = new JSONObject();
        JSONObject httpData = new JSONObject();
        httpData.put("code", 201);
        httpData.put("description", "Created");
        httpResponse.put("http_response", httpData);
        StringRepresentation sRepresentation = new StringRepresentation(httpResponse.toJSONString());
        sRepresentation.setMediaType(MediaType.APPLICATION_JSON);
        response.setEntity(sRepresentation);
        return response;
    }

    public static Response createCreatedResponse(Response response, JSONObject jsonResponse) {
        response.setStatus(Status.SUCCESS_CREATED);
        StringRepresentation sRepresentation = new StringRepresentation(jsonResponse.toJSONString());
        sRepresentation.setMediaType(MediaType.APPLICATION_JSON);
        response.setEntity(sRepresentation);
        return response;
    }

    public static Response createBadRequestResponse(Response response) {
        response.setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
        JSONObject httpResponse = new JSONObject();
        JSONObject httpData = new JSONObject();
        httpData.put("code", 400);
        httpData.put("description", "Bad Request");
        httpResponse.put("http_response", httpData);
        StringRepresentation sRepresentation = new StringRepresentation(httpResponse.toJSONString());
        sRepresentation.setMediaType(MediaType.APPLICATION_JSON);
        response.setEntity(sRepresentation);
        return response;
    }

    public static Response createUnauthorizedResponse(Response response) {
        response.setStatus(Status.CLIENT_ERROR_UNAUTHORIZED);
        JSONObject httpResponse = new JSONObject();
        JSONObject httpData = new JSONObject();
        httpData.put("code", 401);
        httpData.put("description", "Unauthorized");
        httpResponse.put("http_response", httpData);
        StringRepresentation sRepresentation = new StringRepresentation(httpResponse.toJSONString());
        sRepresentation.setMediaType(MediaType.APPLICATION_JSON);
        response.setEntity(sRepresentation);
        return response;
    }

    public static Response createNotFoundResponse(Response response) {
        response.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
        JSONObject httpResponse = new JSONObject();
        JSONObject httpData = new JSONObject();
        httpData.put("code", 404);
        httpData.put("description", "Not Found");
        httpResponse.put("http_response", httpData);
        StringRepresentation sRepresentation = new StringRepresentation(httpResponse.toJSONString());
        sRepresentation.setMediaType(MediaType.APPLICATION_JSON);
        response.setEntity(sRepresentation);
        return response;
    }

    public static Response createNotFoundResponse(Response response, String extra) {
        response.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
        JSONObject httpResponse = new JSONObject();
        JSONObject httpData = new JSONObject();
        httpData.put("code", 404);
        httpData.put("description", "Not Found");
        httpData.put("extra", extra);
        httpResponse.put("http_response", httpData);
        StringRepresentation sRepresentation = new StringRepresentation(httpResponse.toJSONString());
        sRepresentation.setMediaType(MediaType.APPLICATION_JSON);
        response.setEntity(sRepresentation);
        return response;
    }

    public static Response createConflictResponse(Response response) {
        response.setStatus(Status.CLIENT_ERROR_CONFLICT);
        JSONObject httpResponse = new JSONObject();
        JSONObject httpData = new JSONObject();
        httpData.put("code", 409);
        httpData.put("description", "Conflict");
        httpResponse.put("http_response", httpData);
        StringRepresentation sRepresentation = new StringRepresentation(httpResponse.toJSONString());
        sRepresentation.setMediaType(MediaType.APPLICATION_JSON);
        response.setEntity(sRepresentation);
        return response;
    }

    public static Response createInternalServerErrorResponse(Response response) {
        response.setStatus(Status.SERVER_ERROR_INTERNAL);
        JSONObject httpResponse = new JSONObject();
        JSONObject httpData = new JSONObject();
        httpData.put("code", 500);
        httpData.put("description", "Internal Server Error");
        httpResponse.put("http_response", httpData);
        StringRepresentation sRepresentation = new StringRepresentation(httpResponse.toJSONString());
        sRepresentation.setMediaType(MediaType.APPLICATION_JSON);
        response.setEntity(sRepresentation);
        return response;
    }

}
