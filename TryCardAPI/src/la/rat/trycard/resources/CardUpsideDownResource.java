package la.rat.trycard.resources;

import la.rat.trycard.ResponseHelper;
import la.rat.trycard.dataproviders.DataProvider;
import la.rat.trycard.dataproviders.DataProviderFactory;
import la.rat.trycard.exceptions.MyHTTPException;
import la.rat.trycard.model.Game;
import org.restlet.Response;
import org.restlet.data.Form;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

public class CardUpsideDownResource extends ServerResource {
    // comando para probarlo
    // curl -X POST localhost:8080/game/1/card/1/upsidedown/1?player=1 | python3 -m json.tool
    @Post
    public Response setUpsideDownCard() {
        // obtenemos el los parámetros
        Form form = getRequest().getResourceRef().getQueryAsForm();
        int gameId;
        int playerId;
        int cardId;
        int upsideDownPosition;
        try {
            gameId = Integer.parseInt((String) getRequest().getAttributes().get("game_id"));
            playerId = Integer.parseInt(form.getFirstValue("player"));

            cardId = Integer.parseInt((String) getRequest().getAttributes().get("card_id"));
            upsideDownPosition = Integer.parseInt((String) getRequest().getAttributes().get("upside_down_position"));
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
            return ResponseHelper.createBadRequestResponse(getResponse());
        }

        // cambiamos el estado de la carta
        DataProvider dataProvider = DataProviderFactory.getDataProvider();
        Game updatedGame = null;
        try {
            updatedGame = dataProvider.setUpsideDownCard(gameId, playerId, cardId, upsideDownPosition);
        } catch (MyHTTPException e) {
            switch (e.getErrorCode()) {
                case 500:
                    return ResponseHelper.createInternalServerErrorResponse(getResponse());
            }
        }

        if (updatedGame != null) {
            return ResponseHelper.createCreatedResponse(getResponse(), updatedGame.toJSONObject());
        } else {
            return ResponseHelper.createInternalServerErrorResponse(getResponse());
        }
    }
}