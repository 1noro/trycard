package la.rat.trycard.resources;

import la.rat.trycard.dataproviders.DataProvider;
import la.rat.trycard.dataproviders.DataProviderFactory;
import la.rat.trycard.ResponseHelper;
import la.rat.trycard.exceptions.MyHTTPException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.restlet.Response;
import org.restlet.data.Form;
import org.restlet.representation.Representation;
import org.restlet.resource.Delete;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import java.io.IOException;

public class GameStatusResource extends ServerResource {
    // comando para probarlo
    // curl -X POST localhost:8080/game -H "Content-Type: application/json" -d '{"key" : "value"}'
    // para cargar archivos:
    // curl -X POST localhost:8080/game -H "Content-Type: application/json" -d @requestBody.json
    @Post("json")
    public Response createGame(Representation requestBody) {
        // obtenemos el requestBody
        JSONArray gameDecksJSON = null;
        try {
            JSONParser parser = new JSONParser();
            gameDecksJSON = (JSONArray) parser.parse(requestBody.getText());
        } catch (IOException | ParseException e) {
            e.printStackTrace();
            return ResponseHelper.createBadRequestResponse(getResponse());
        }

        // creamos la partida (GAME)
        DataProvider dataProvider = DataProviderFactory.getDataProvider();
        Integer gameId = null;
        try {
            gameId = dataProvider.createGame(gameDecksJSON);
        } catch (MyHTTPException e) {
            switch (e.getErrorCode()) {
                case 500:
                    return ResponseHelper.createInternalServerErrorResponse(getResponse());
            }
        }
        if (gameId != null) {
            JSONObject jsonResponse = new JSONObject();
            jsonResponse.put("gameId", gameId);
            return ResponseHelper.createCreatedResponse(getResponse(), jsonResponse);
        } else {
            return ResponseHelper.createInternalServerErrorResponse(getResponse());
        }
    }

    // comando para probarlo
    // curl -X DELETE localhost:8080/game?id=1
    @Delete
    public Response endGame() {
        Form form = getRequest().getResourceRef().getQueryAsForm();

        int gameId;
        try {
            gameId = Integer.parseInt(form.getFirstValue("id"));
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
            return ResponseHelper.createBadRequestResponse(getResponse());
        }

        DataProvider dataProvider = DataProviderFactory.getDataProvider();
        int httpCode = 0;
        try {
            httpCode = dataProvider.endGame(gameId);
        } catch (MyHTTPException e) {
            switch (e.getErrorCode()) {
                case 500:
                    return ResponseHelper.createInternalServerErrorResponse(getResponse());
            }
        }
        if (httpCode == 200) {
            return ResponseHelper.createSuccessResponse(getResponse(), "game deleted");
        } else {
            return ResponseHelper.createInternalServerErrorResponse(getResponse());
        }
    }
}
