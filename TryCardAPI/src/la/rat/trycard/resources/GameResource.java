package la.rat.trycard.resources;

import la.rat.trycard.dataproviders.DataProvider;
import la.rat.trycard.dataproviders.DataProviderFactory;
import la.rat.trycard.ResponseHelper;
import la.rat.trycard.exceptions.MyHTTPException;
import org.json.simple.JSONObject;
import org.restlet.Response;
import org.restlet.data.Form;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

public class GameResource extends ServerResource {
    // comando para probarlo
    // curl -X GET localhost:8080/game/1?player=1 | python3 -m json.tool
    @Get
    public Response getGame() {
        Form form = getRequest().getResourceRef().getQueryAsForm();
        int gameId;
        int playerId;
        try {
            gameId = Integer.parseInt((String) getRequest().getAttributes().get("game_id"));
            playerId = Integer.parseInt(form.getFirstValue("player"));
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
            return ResponseHelper.createBadRequestResponse(getResponse());
        }

        DataProvider dataProvider = DataProviderFactory.getDataProvider();
        JSONObject jsonObject = null;
        try {
            jsonObject = dataProvider.getGame(gameId, playerId).toJSONObject();
        } catch (MyHTTPException e) {
            switch (e.getErrorCode()) {
                case 500:
                    return ResponseHelper.createInternalServerErrorResponse(getResponse());
            }
        }
        assert jsonObject != null;
        return ResponseHelper.createSuccessResponse(getResponse(), jsonObject);
    }
}
