package la.rat.trycard.resources;

import la.rat.trycard.ResponseHelper;
import la.rat.trycard.dataproviders.DataProvider;
import la.rat.trycard.dataproviders.DataProviderFactory;
import la.rat.trycard.exceptions.MyHTTPException;
import la.rat.trycard.model.Game;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.restlet.Response;
import org.restlet.data.Form;
import org.restlet.representation.Representation;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import java.io.IOException;

public class PlayerLifesResource extends ServerResource {
    // comando para probarlo
    // curl -X POST localhost:8080/game/1/lifes?player=1 -H "Content-Type: application/json" -d @plusLifes.json | python3 -m json.tool
    @Post
    public Response setPlayerLifes(Representation requestBody) {
        // obtenemos el los parámetros
        Form form = getRequest().getResourceRef().getQueryAsForm();
        int gameId;
        int playerId;
        try {
            gameId = Integer.parseInt((String) getRequest().getAttributes().get("game_id"));
            playerId = Integer.parseInt(form.getFirstValue("player"));
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
            return ResponseHelper.createBadRequestResponse(getResponse());
        }

        // obtenemos el requestBody y sus valores
        JSONObject cardNewLifesJSON = null;
        int plusLifes;
        try {
            JSONParser parser = new JSONParser();
            cardNewLifesJSON = (JSONObject) parser.parse(requestBody.getText());
            plusLifes = ((Long) cardNewLifesJSON.get("plus_lifes")).intValue();
        } catch (IOException | ParseException | NumberFormatException e) {
            e.printStackTrace();
            return ResponseHelper.createBadRequestResponse(getResponse());
        }

        // cambiamos el estado de la carta
        DataProvider dataProvider = DataProviderFactory.getDataProvider();
        Game updatedGame = null;
        try {
            updatedGame = dataProvider.setPlayerLifes(gameId, playerId, plusLifes);
        } catch (MyHTTPException e) {
            switch (e.getErrorCode()) {
                case 500:
                    return ResponseHelper.createInternalServerErrorResponse(getResponse());
            }
        }

        if (updatedGame != null) {
            return ResponseHelper.createCreatedResponse(getResponse(), updatedGame.toJSONObject());
        } else {
            return ResponseHelper.createInternalServerErrorResponse(getResponse());
        }
    }
}