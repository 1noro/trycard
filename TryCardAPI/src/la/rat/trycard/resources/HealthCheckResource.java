package la.rat.trycard.resources;

import la.rat.trycard.ResponseHelper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.restlet.Response;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import java.io.IOException;

public class HealthCheckResource extends ServerResource {
    @Get
    public Response getHealth() {
        JSONObject healthMsg = new JSONObject();
        healthMsg.put("text", "Health Check OK");
        return ResponseHelper.createSuccessResponse(getResponse(), healthMsg);
    }
    @Post("json")
    public Response getHealth(Representation requestBody) {
        // obtenemos el requestBody
        JSONObject requestBodyJSON = null;
        try {
            JSONParser parser = new JSONParser();
            requestBodyJSON = (JSONObject) parser.parse(requestBody.getText());
        } catch (IOException | ParseException e) {
            e.printStackTrace();
            return ResponseHelper.createBadRequestResponse(getResponse());
        }

        // creamos la response
        JSONObject healthMsg = new JSONObject();
        healthMsg.put("text_response", requestBodyJSON.get("text_request"));
        return ResponseHelper.createSuccessResponse(getResponse(), healthMsg);
    }
}

