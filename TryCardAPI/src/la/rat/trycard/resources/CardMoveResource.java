package la.rat.trycard.resources;

import la.rat.trycard.ResponseHelper;
import la.rat.trycard.dataproviders.DataProvider;
import la.rat.trycard.dataproviders.DataProviderFactory;
import la.rat.trycard.exceptions.MyHTTPException;
import la.rat.trycard.model.Game;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.restlet.Response;
import org.restlet.data.Form;
import org.restlet.representation.Representation;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import java.io.IOException;

public class CardMoveResource extends ServerResource {
    // comando para probarlo
    // curl -X POST localhost:8080/game/1/card/1/move?player=1 -H "Content-Type: application/json" -d @cardNewPosition.json | python3 -m json.tool
    @Post
    public Response moveCard(Representation requestBody) {
        // obtenemos el los parámetros
        Form form = getRequest().getResourceRef().getQueryAsForm();
        int gameId;
        int playerId;
        int cardId;
        try {
            gameId = Integer.parseInt((String) getRequest().getAttributes().get("game_id"));
            playerId = Integer.parseInt(form.getFirstValue("player"));

            cardId = Integer.parseInt((String) getRequest().getAttributes().get("card_id"));
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
            return ResponseHelper.createBadRequestResponse(getResponse());
        }

        // obtenemos el requestBody y sus valores
        JSONObject cardNewPositionJSON = null;
        int xPos;
        int yPos;
        int zPos;
        try {
            JSONParser parser = new JSONParser();
            cardNewPositionJSON = (JSONObject) parser.parse(requestBody.getText());
            xPos = ((Long) cardNewPositionJSON.get("x_pos")).intValue();
            yPos = ((Long) cardNewPositionJSON.get("y_pos")).intValue();
            zPos = ((Long) cardNewPositionJSON.get("z_pos")).intValue();
        } catch (IOException | ParseException | NumberFormatException e) {
            e.printStackTrace();
            return ResponseHelper.createBadRequestResponse(getResponse());
        }

        // cambiamos el estado de la carta
        DataProvider dataProvider = DataProviderFactory.getDataProvider();
        Game updatedGame = null;
        try {
            updatedGame = dataProvider.moveCard(gameId, playerId, cardId, xPos, yPos, zPos);
        } catch (MyHTTPException e) {
            switch (e.getErrorCode()) {
                case 500:
                    return ResponseHelper.createInternalServerErrorResponse(getResponse());
            }
        }

        if (updatedGame != null) {
            return ResponseHelper.createCreatedResponse(getResponse(), updatedGame.toJSONObject());
        } else {
            return ResponseHelper.createInternalServerErrorResponse(getResponse());
        }
    }
}