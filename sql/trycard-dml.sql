-- Test de funcionamiento

INSERT INTO DECK (name) VALUES ('Negro');
INSERT INTO DECK (name) VALUES ('Verde');

INSERT INTO CARD (name, image_path, deck_id) VALUES ('Nicol Bolas', 'https://imgur.com/ghf736478.png', 1);
INSERT INTO CARD (name, image_path, deck_id) VALUES ('Raizguana', 'https://imgur.com/kjdas238.png', 2);

INSERT INTO GAME (player1_deck_id, player2_deck_id) VALUES (1, 2);


SELECT * FROM DECK;
SELECT * FROM CARD;
SELECT * FROM GAME;


DELETE FROM GAME;
DELETE FROM CARD;
DELETE FROM DECK;


-- DROP TABLE GAME;
-- DROP TABLE CARD;
-- DROP TABLE DECK;