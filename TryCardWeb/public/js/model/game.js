class Game {
    constructor (id, myDeck, opponentDeck, myLifes, opponentLifes) {
        this.id = id;
        this.myDeck = myDeck;
        this.opponentDeck = opponentDeck;
        this.myLifes = myLifes;
        this.opponentLifes = opponentLifes;
    }
}