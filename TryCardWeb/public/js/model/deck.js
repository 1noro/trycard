class Deck {
    constructor (id, name, cardsInLibrary, cardsInHand, cardsInGame) {
        this.id = id;
        this.name = name;
        this.cardsInLibrary = cardsInLibrary;
        this.cardsInHand = cardsInHand;
        this.cardsInGame = cardsInGame;
    }
}