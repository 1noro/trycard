
class Card {
    constructor (id, name, imagePath, tapped, upsideDown, plusAttack, plusDefense, xPos, yPos, zPos) {
        this.id = id;
        this.name = name;
        this.imagePath = imagePath;
        this.tapped = tapped;
        this.upsideDown = upsideDown;
        this.plusAttack = plusAttack;
        this.plusDefense = plusDefense;
        this.xPos = xPos;
        this.yPos = yPos;
        this.zPos = zPos;
    }
}