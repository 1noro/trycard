function isValidHttpUrl(string) {
    let url;

    try {
        url = new URL(string);
    } catch (_) {
        return false;
    }

    return url.protocol === "http:" || url.protocol === "https:";
}

class CardDefinition {
    constructor (name, image_path, quantity) {
        this.name = name;
        this.image_path = encodeURI(image_path);
        this.quantity = quantity;
    }

    get isValid() {
        var valid = true;
        if (
            typeof this.name != 'string' ||
            typeof this.image_path != 'string' ||
            typeof this.quantity != 'number' ||
            !isValidHttpUrl(this.image_path)
        ) {
            valid = false;
        }
        return valid;
    }
}

class DeckDefinition {
    constructor (name, mainList) {
        this.name = name;
        this.main = mainList;
    }

    get isValid() {
        var valid = true;
        if (typeof this.name != 'string' || this.main.length == 0) {
            valid = false;
        }
        this.main.forEach(card => {
            if (!card.isValid) {
                valid = false;
            }
        });
        return valid;
    }

}