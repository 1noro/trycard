
var game;

function serialize(gameJSONText) {
    var gameJSON = JSON.parse(gameJSONText);

    var myCardsInLibray = [];
    gameJSON.my_deck.cards_in_library.forEach(cardJSON => {
        myCardsInLibray.push(new Card(cardJSON.id, cardJSON.name, cardJSON.image_path, cardJSON.tapped, cardJSON.upside_down, cardJSON.plus_attack, cardJSON.plus_defense, cardJSON.x_pos, cardJSON.y_pos, cardJSON.z_pos))
    });
    var myCardsInHand = [];
    gameJSON.my_deck.cards_in_hand.forEach(cardJSON => {
        myCardsInHand.push(new Card(cardJSON.id, cardJSON.name, cardJSON.image_path, cardJSON.tapped, cardJSON.upside_down, cardJSON.plus_attack, cardJSON.plus_defense, cardJSON.x_pos, cardJSON.y_pos, cardJSON.z_pos))
    });
    var myCardsInGame = [];
    gameJSON.my_deck.cards_in_game.forEach(cardJSON => {
        myCardsInGame.push(new Card(cardJSON.id, cardJSON.name, cardJSON.image_path, cardJSON.tapped, cardJSON.upside_down, cardJSON.plus_attack, cardJSON.plus_defense, cardJSON.x_pos, cardJSON.y_pos, cardJSON.z_pos))
    });

    var opponentCardsInLibray = [];
    gameJSON.opponent_deck.cards_in_library.forEach(cardJSON => {
        opponentCardsInLibray.push(new Card(cardJSON.id, cardJSON.name, cardJSON.image_path, cardJSON.tapped, cardJSON.upside_down, cardJSON.plus_attack, cardJSON.plus_defense, cardJSON.x_pos, cardJSON.y_pos, cardJSON.z_pos))
    });
    var opponentCardsInHand = [];
    gameJSON.opponent_deck.cards_in_hand.forEach(cardJSON => {
        opponentCardsInHand.push(new Card(cardJSON.id, cardJSON.name, cardJSON.image_path, cardJSON.tapped, cardJSON.upside_down, cardJSON.plus_attack, cardJSON.plus_defense, cardJSON.x_pos, cardJSON.y_pos, cardJSON.z_pos))
    });
    var opponentCardsInGame = [];
    gameJSON.opponent_deck.cards_in_game.forEach(cardJSON => {
        opponentCardsInGame.push(new Card(cardJSON.id, cardJSON.name, cardJSON.image_path, cardJSON.tapped, cardJSON.upside_down, cardJSON.plus_attack, cardJSON.plus_defense, cardJSON.x_pos, cardJSON.y_pos, cardJSON.z_pos))
    });

    var myDeck = new Deck(gameJSON.my_deck.id, gameJSON.my_deck.name, myCardsInLibray, myCardsInHand, myCardsInGame);
    var opponentDeck = new Deck(gameJSON.opponent_deck.id, gameJSON.opponent_deck.name, opponentCardsInLibray, opponentCardsInHand, opponentCardsInGame);
    game = new Game(gameJSON.id, myDeck, opponentDeck, gameJSON.my_lifes, gameJSON.opponent_lifes);
    // console.log(game);
}

function main() {
    // console.log("main");

    // obtenemos los elementos del HTML que hay que modificar
    var gameIdInput = document.getElementById("game_id");
    var playerIdInput = document.getElementById("player_id");
    var player2LinkContentP = document.getElementById("player2_link_content");
    var player2LinkTextInput = document.getElementById("player2_link_text");
    var player2LinkCopyButton = document.getElementById("player2_link_copy_button");

    var yourDeckTitle = document.getElementById("your_deck_title");
    var yourDeckContent = document.getElementById("your_deck_content");
    var yourLibraryP = document.getElementById("your_library");
    var yourHandP = document.getElementById("your_hand");
    var yourGameP = document.getElementById("your_game");
    var hideYourDeckButton = document.getElementById("hide_your_deck_button");


    // Obtenemos los query params
    const urlParams = new URLSearchParams(window.location.search);
    const gameId = urlParams.get('game');
    const playerId = urlParams.get('player');

    yourDeckTitle.addEventListener("click", function() {
        if (yourDeckContent.style.display == "none") {
            yourDeckContent.style.display = "block";
            this.innerText = "- Tu mazo";
        } else {
            yourDeckContent.style.display = "none";
            this.innerText = "+ Tu mazo";
        }
    });

    hideYourDeckButton.addEventListener("click", function() {
        yourDeckContent.style.display = "none";
        yourDeckTitle.innerText = "+ Tu mazo";
    });

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // document.getElementById("demo").innerHTML = this.responseText;
            console.log(this.status);
            // console.log(this.responseText);

            serialize(this.responseText);

            // console.log(game.myDeck.cardsInLibrary);
            createCardsPreview(yourLibraryP, game.myDeck.cardsInLibrary);
            createCardsPreview(yourHandP, game.myDeck.cardsInHand);
            createCardsPreview(yourGameP, game.myDeck.cardsInGame);

            gameIdInput.value = game.id;
            playerIdInput.value = playerId;
            if (playerId != 1) {
                player2LinkContentP.style.display = "none";
            } else {
                var theURL = window.location.protocol + "//" + window.location.host + "/" + window.location.pathname + "?game=" + game.id + "&player=2";
                player2LinkTextInput.value = theURL;
                player2LinkCopyButton.addEventListener("click", function() {
                    /* Select the text field */
                    player2LinkTextInput.select();
                    // player2LinkTextInput.setSelectionRange(0, 99999); // For mobile devices

                    /* Copy the text inside the text field */
                    document.execCommand("copy");

                    /* Alert the copied text */
                    // alert("Copied the text: " + player2LinkTextInput.value);
                });
            }
        }
    };
    xhttp.open("GET", "http://192.168.111.111:8080/game/" + gameId + "?player=" + playerId, true);
    xhttp.send();

}