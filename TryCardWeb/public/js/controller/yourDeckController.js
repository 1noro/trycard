
function hideBigger(imagePath) {
    // console.log("out");
    var bigCard = document.getElementById("big_card");
    bigCard.style.display = "none";
}

function createCardsPreview(HTMLSection, cardsArray) {
    if (cardsArray.length > 0) {
        HTMLSection.innerHTML = "";
        cardsArray.forEach(card => {
            var cardImg = document.createElement("img");
            cardImg.src = card.imagePath;
            cardImg.style.width = "100px";
            cardImg.addEventListener("mouseover", function() {
                // console.log("in: " + imagePath);
                var bigCard = document.getElementById("big_card");
                bigCard.src = card.imagePath;
                bigCard.style.display = "block";
            });
            cardImg.addEventListener("mouseout", hideBigger);
            HTMLSection.appendChild(cardImg);
        });
    }
}