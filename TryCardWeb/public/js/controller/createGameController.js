function buildDeckDefinition(deckCode) {
    let regex = /(\d{1,3}) (.*) (http.*\.jpg|http.*\.png)/gm;
    var lines = deckCode.split("\n");
    var mainList = [];
    while ((m = regex.exec(deckCode)) !== null) {
        // This is necessary to avoid infinite loops with zero-width matches
        if (m.index === regex.lastIndex) {
            regex.lastIndex++;
        }
        mainList.push(new CardDefinition(m[2], m[3], parseInt(m[1])));
    }
    // console.log(mainList);
    // console.log(new DeckDefinition(lines[0].replace("//NAME: ", ""), mainList));
    return new DeckDefinition(lines[0].replace("//NAME: ", ""), mainList);
}

function createGame() {
    var player1DeckDefinition = buildDeckDefinition(document.getElementById("player1_deck").value);
    var player2DeckDefinition = buildDeckDefinition(document.getElementById("player2_deck").value);
    if (player1DeckDefinition.isValid && player2DeckDefinition.isValid) {
        console.log("OK");
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && (this.status == 200 || this.status == 201)) {
                // document.getElementById("demo").innerHTML = this.responseText;
                console.log(this.status);
                console.log(this.responseText);
                window.location.href = 'game.html?game=' + JSON.parse(this.responseText).gameId + '&player=1'; //relative to domain
            }
            // console.log(this.readyState);
            // console.log(this.status);
            // console.log(this.responseText);
        };
        xhttp.open("POST", "http://192.168.111.111:8080/game", true);
        xhttp.setRequestHeader("Content-type", "application/json");
        xhttp.send(JSON.stringify([player1DeckDefinition, player2DeckDefinition]));
    } else {
        // console.log("Error");
        var mainSection = document.getElementById("main");
        var errorParagraph = document.createElement("p");
        errorParagraph.innerHTML = '<span style="color: red;"><small>Código no válido.</small></span>';
        mainSection.appendChild(errorParagraph);
    }
}